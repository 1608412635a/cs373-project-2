.PHONY: IDB3.log

FILES :=                              \
	.gitignore                      \
	.gitlab-ci.yml                       \
	models.py                     \
	tests.py                     \
	templates/index.html                   \
	IDB3.log                     \
	postman.YAML                        \
	models.html                     \
	UML.pdf                     \


ifeq ($(shell uname), Darwin)          # Apple
	PYTHON   := python3
	PIP      := pip3
	PYLINT   := pylint
	COVERAGE := coverage
	PYDOC    := pydoc3
	AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
	PYTHON   := python                 
	PIP      := pip3
	PYLINT   := pylint
	COVERAGE := coverage
	PYDOC    := python -m pydoc        
	AUTOPEP8 := autopep8
else                                   # UTCS
	PYTHON   := python3
	PIP      := pip3
	PYLINT   := pylint3
	COVERAGE := coverage
	PYDOC    := pydoc3
	AUTOPEP8 := autopep8
endif

models.html: models.py
	$(PYDOC) -w models

IDB1.log:
	git log > IDB1.log

IDB2.log:
	git log > IDB2.log

IDB3.log:
	git log > IDB3.log

cProfile:
	$(PYTHON) -m cProfile models.py

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  tests.tmp
	rm -rf __pycache__

config:
	git config -l

format:
	$(AUTOPEP8) -i models.py

run:            # make sure your env or venv is running
	make check
	$(PYTHON) models.py

scrub:
	make clean
	rm -f  models.html
	rm -f  IDB1.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

tests.tmp: tests.py models.py
	$(COVERAGE) run    --branch tests.py >  tests.tmp 2>&1
	$(COVERAGE) report -m                      >> tests.tmp
	cat tests.tmp

test: tests.tmp check

versions:
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version

fulldoc:
	make clean
	make models.html
	git log > IDB3.log