import * as d3 from 'd3';
import React, { useRef, useEffect } from 'react';

function BarChart({ width, height, data }){
    const ref = useRef();

    useEffect(() => {
        const svg = d3.select(ref.current)
            .attr("width", width)
            .attr("height", height)
            .style("border", "1px solid black")
    }, []);

    useEffect(() => {
        draw();
    }, [data]);

    const draw = () => {
        
        const svg = d3.select(ref.current);
        //var selection = svg.selectAll("rect").data(data);
        var xScale = d3.scaleBand()
        .domain(data.map( (d) => d.name))
        .range([0, width])
        .padding(0.4);
        var yScale = d3.scaleLinear()
                            .domain([0, d3.max(data.name)])
                            .range([height, 0]);
                        
        var g = svg.append("g")
                    .attr("transform", "translate(" + 100 + "," + 100 + ")");
        
        g.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(xScale));

        g.append("g")
            .call(d3.axisLeft(yScale).tickFormat(function(d){
                return d;
            }).ticks(10))
            .append("text")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .text("value");

        // selection
        //     .enter()
        //     .append("rect")
        //     .attr("x", (d, i) => i * 45)
        //     //.attr("y", (d) => height)
        //     .attr("width", 40)
        //     //.attr("height", 0)
        //     .attr("fill", "orange")
        //     .attr("height", (d) => yScale(d))
        //     .attr("y", (d) => height - yScale(d))
        //     //.attr("width", (d) => xScale(d))
    }


    return (
        <div className="chart">
            <svg ref={ref}>
            </svg>
        </div>
        
    )

}

export default BarChart;