import React, { useState, useEffect } from "react";
import { Image, Table, Card, Button } from 'react-bootstrap';
import matthew from '../Matthew.png';
import sam from '../Sam.jpg';
import wentao from '../Wentao.jpg';
import yinglei from '../Yinglei.jpg';
import lauren from '../Lauren.jpg';

function AboutPage(props) {
  const [utest, setUtest] = useState("");
  useEffect(() => {
    fetch("/api/runtests").then(response =>
        response.json().then(data => {
          console.log(data.text);
          setUtest(data.text);
        })
    );
  }, []);

  const[showMessage, setShowMessage] = useState(false);
  
  return(<div>
  <Button onClick={() => {setShowMessage(true);}}>[ CLICK HERE TO RUN UNIT TESTS ]</Button>
  {showMessage && <p>{utest}</p>}
  <Card>
      <Table>
          <tr>
              <th>Number of Commits</th>
              <td>162</td>
          </tr>
          <tr>
              <th>Number of Issues</th>
              <td>50</td>
          </tr>
          <tr>
              <th>Number of Unit Tests</th>
              <td>18</td>
          </tr>
          <tr>
              <td><a href="https://documenter.getpostman.com/view/11832684/T17M5jp7">Postman API</a></td>
              <td><a href="https://gitlab.com/lmw2997/cs373-project-2/-/issues">GitLab Issues Tracker</a></td>
          </tr>
          <tr>
              <td><a href="https://gitlab.com/lmw2997/cs373-project-2">GitLab Repo</a></td>
              <td><a href="https://gitlab.com/lmw2997/cs373-project-2/-/wikis/home">GitLab Wiki</a></td>
          </tr>
          <tr>
              <td><a href="https://speakerdeck.com/thelauracle/cs373-group-3-rockin-with-the-rona">SpeakDeck Presentation</a></td>
          </tr>
      </Table>
    </Card>  
    <Card>  
        <Table>
            <tr>
                <th>Data Sources</th>
                <th>Description</th>
            </tr>
            <tr>
                <th><a href="https://developer.spotify.com/documentation/web-api/">Spotify</a></th>
                <th>Spotify's Web API, scraped using a Python/SQLAlchemy script</th>
            </tr>
        </Table>
    </Card>
    <Card>
        <Table>
            <tr>
                <th>Frontend Tools Used</th>
                <td>React Bootstrap</td>
                <td>HTML/CSS/JS</td>
            </tr>
            <tr>
                <th>Backend Tools Used</th>
                <td>Flask</td>
                <td>SQLAlchemy</td>
            </tr>
            <tr>
                <th>Design Tools Used</th>
                <td>Postman API</td>
                <td>UML (Diagrams)</td>
            </tr>
            <tr>
                <th>Database/Hosting Tools Used</th>
                <td>Postgresql</td>
                <td>Amazon Web Services</td>
            </tr>
        </Table>
    </Card>
    <ul class="memberList">
        <li>
            <Card className="text-center">
                <Card.Header><Image rounded src={sam} width="360" height="380" alt="Sam_Picture" /></Card.Header>
                <p className="text-center">
                    Sam Turner<br/>
                    CS Major<br/>
                    API<br/>
                    No. of Commits: 33<br/>
                    No. of Issues: 5<br/>
                    No. of Tests: 0
                </p>
            </Card>
        </li>
        <li>
            <Card className="text-center">
                <Card.Header><Image rounded src={matthew} width="290" height="380" alt="Matthew_Picture" /></Card.Header>
                <p className="text-center">
                    Matthew Armour<br/>
                    CS Major<br/>
                    AWS Hosting<br/>
                    No. of Commits: 3<br/>
                    No. of Issues: 6<br/>
                    No. of Tests: 0
                </p>
            </Card>
        </li>
        <li>
            <Card className="text-center">
                <Card.Header><Image rounded src={yinglei}  width="290" height="380" alt="Yinglei_Picture" /></Card.Header>
                <p className="text-center">
                    Yinglei Fang<br/>
                    CS Major<br/>
                    Frontend<br/>
                    No. of Commits: 7<br/>
                    No. of Issues: 6<br/>
                    No. of Tests: 0
                </p>
            </Card>
        </li>
        <li>
            <Card className="text-center">
                <Card.Header><Image rounded src={wentao} width="320" height="380" alt="Wentao_Picture" /></Card.Header>
                <p className="text-center">
                    Wentao Yang<br/>
                    CS Major<br/>
                    Backend<br/>
                    No. of Commits: 32<br/>
                    No. of Issues: 10<br/>
                    No. of Tests: 18
                </p>
            </Card>
        </li>
        <li>
            <Card className="text-center">
                <Card.Header><Image rounded src={lauren} width="380" height="380" alt="Lauren_Picture" /></Card.Header>
                <p className="text-center">
                    Lauren Wang<br/>
                    CS Major, Philosophy of Law Minor<br/>
                    React Bootstrap, Team Lead<br/>
                    No. of Commits: 87<br/>
                    No. of Issues: 24<br/>
                    No. of Tests: 0<br/>
                    (Disclaimer: these numbers may look unbalanced because of merge requests / administrative issues)
                </p>
            </Card>
        </li>
    </ul>
</div>);
}

export default AboutPage;
