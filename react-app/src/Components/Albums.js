import React, { useEffect, useState } from "react";
//import { Table } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import MaterialTable from 'material-table';

function Albums(props) {
  const history = useHistory();
  const routeChange = (pathName, rowData) =>{ 
    let path = pathName;
    history.push({
      pathname: path,
      state: { data: rowData},
    });
  }

  const [state, setState] = useState({
    columns: [
      { title: 'Album Name', field: 'album_name' },
      { title: 'Artist', field: 'album_artists[0].artist_name' },
      { title: 'Number of Tracks', field: 'album_numtracks', type: 'numeric'},
      { title: 'Release Date', field: 'album_release_day'},
      { title: 'Spotify URL', field: 'album_spotify_url' },
    ],
    data: [],
  });
  useEffect(() => {
    fetch('/api/albums?name=mic').then(res => res.json()).then(data => {
      setState({
        columns: [
          { title: 'Album Name', field: 'album_name' },
          { title: 'Artist', field: 'album_artists[0].artist_name' },
          { title: 'Number of Tracks', field: 'album_numtracks', type: 'numeric'},
          { title: 'Release Date', field: 'album_release_day'},
          { title: 'Spotify URL', field: 'album_spotify_url' },
        ],
        data: data.Albums,
      });
    });
  }, []);

    return (
    <div>
    <MaterialTable
      title="Albums"
      columns={state.columns}
      data={state.data}
      onRowClick={(event, rowData) => {
        routeChange("/albumInstance", rowData);
      }}
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
    </div>
  );
}

export default Albums;
