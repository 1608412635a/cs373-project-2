import React, { useEffect, useState } from "react";
//import { Table } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import MaterialTable from 'material-table';

function Artists() {

  const history = useHistory();
  const routeChange = (pathName, rowData) =>{ 
    let path = pathName;
    history.push({
      pathname: path,
      state: { data: rowData},
    });
  }
  const [state, setState] = useState({
    columns: [
      { title: 'Name', field: 'artist_name' },
      { title: 'ID', field: 'artist_id', type: 'numeric' },
      { title: 'Genres', field: 'artist_genres[0]'},
      { title: 'Spotify ID', field: 'artist_spotify_id' },
      { title: 'Spotify URL', field: 'artist_spotify_url'}
    ],
    data: [],
  });
  useEffect(() => {
    fetch('/api/artists?name=mi').then(res => res.json()).then(data => {
      setState({
        columns: [
          { title: 'Name', field: 'artist_name' },
          { title: 'ID', field: 'artist_id', type: 'numeric' },
          { title: 'Genres', field: 'artist_genres[0]'},
          { title: 'Spotify ID', field: 'artist_spotify_id' },
          { title: 'Spotify URL', field: 'artist_spotify_url'}
        ],
        data: data.Artists,
      });
    });
  }, []);

    return (
    <React.Fragment>
    <MaterialTable
      title="Artists"
      columns={state.columns}
      data={state.data}
      onRowClick={(event, rowData) => {
        routeChange("/artistInstance", rowData);
      }}
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
    </React.Fragment>
  );
}

export default Artists;
